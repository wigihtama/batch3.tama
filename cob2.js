import {
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  Linking,
  ScrollView,
} from 'react-native';

const Iklan = () => {
  const handleAdClick = () => {
    Linking.openURL('https://business.instagram.com/advertising?locale=id_ID');
    Linking.openURL('https://lenteramata.com/contoh-iklan-layanan-masyarakat/');
    Linking.openURL(
      'https://www.disrupto.co.id/journal/maraknya-kemunculan-bank-digital-di-indonesia',
    );
    Linking.openURL(
      'https://www.bizadmark.com/advertising-for-banking-sector/',
    );
    Linking.openURL(
      'https://blockchair.com/id/news/the-crypto-bank-delays-sec-financial-report-silvergate-stock-plummets-31--86eb0d0872',
    );
  };
  return (
    <ScrollView horizontal={true}>
      <View style={styles.jarak}>
        <TouchableOpacity style={styles.adContainer} onPress={handleAdClick}>
          <View style={styles.adImageContainer}>
            <Image
              style={styles.adImage}
              source={{
                uri: 'https://www.lummoshop.co.id/wp-content/uploads/2022/04/01-28.jpg',
              }}
            />
          </View>
          <View style={styles.adContent}></View>
        </TouchableOpacity>
        <TouchableOpacity style={styles.adContainer} onPress={handleAdClick}>
          <View style={styles.adImageContainer}>
            <Image
              style={styles.adImage}
              source={{
                uri: 'https://i0.wp.com/lenteramata.com/wp-content/uploads/2021/07/Contoh-Desain-Gambar-Iklan-Layanan-Masyarakat-Tentang-Lingkungan.jpg?resize=650%2C350&ssl=1',
              }}
            />
          </View>
          <View style={styles.adContent}></View>
        </TouchableOpacity>
        <TouchableOpacity style={styles.adContainer} onPress={handleAdClick}>
          <View style={styles.adImageContainer}>
            <Image
              style={styles.adImage}
              source={{
                uri: 'https://images.squarespace-cdn.com/content/v1/5e4a317804dca34b427b4732/1622027065479-M1UZFDF6YG49GCYLEMYM/Disrupto-2021-May-Persaingan-Digital-Bank-Web.jpg',
              }}
            />
          </View>
          <View style={styles.adContent}></View>
        </TouchableOpacity>
        <TouchableOpacity style={styles.adContainer} onPress={handleAdClick}>
          <View style={styles.adImageContainer}>
            <Image
              style={styles.adImage}
              source={{
                uri: 'https://www.bizadmark.com/wp-content/uploads/2021/05/banks-ads.gif',
              }}
            />
          </View>
          <View style={styles.adContent}></View>
        </TouchableOpacity>
        <TouchableOpacity style={styles.adContainer} onPress={handleAdClick}>
          <View style={styles.adImageContainer}>
            <Image
              style={styles.adImage}
              source={{
                uri: 'https://blockchair.com/news/the-crypto-bank-delays-sec-financial-report-silvergate-stock-plummets-31--86eb0d0872.jpg',
              }}
            />
          </View>
          <View style={styles.adContent}></View>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  adContainer: {
    backgroundColor: '#FFCC02',
    alignSelf: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    overflow: 'hidden',
    height: 190,
    marginTop: 15,
    marginBottom: 15,
    width: 330,
    borderRadius: 15,
    marginLeft: 10,
  },
  adImageContainer: {
    backgroundColor: '#FFCC02',
    alignSelf: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    overflow: 'hidden',
    height: 200,
    marginTop: 25,
    marginBottom: 15,
    width: 350,
    borderRadius: 15,
  },
  adImage: {
    width: '100%',
    height: '100%',
    resizeMode: 'cover',
  },
  adContent: {
    flex: 1,
    padding: 10,
    justifyContent: 'center',
  },
  jarak: {flexDirection: 'row', marginRight: 20},
});

export default Iklan;
