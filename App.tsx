import React from 'react'
import storeRedux from './src/redux/Store'
import { Provider } from 'react-redux'
import Router from './src/route/Route'


const App = () => {
  return (
    
    <Provider store={storeRedux}>
        <Router />
    </Provider>

    )
}

export default App
