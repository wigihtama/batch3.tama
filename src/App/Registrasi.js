import React, {useState} from 'react';
import {
  Image,
  StyleSheet,
  ImageBackground,
  Text,
  TextInput,
  Modal,
  ToastAndroid,
  TouchableOpacity,
  View,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import icGrab from '../icons/segitiga1.png';
import icBackground from '../icons/4.png';
import {Register} from '../redux/action/Action';

const Registrasi = ({navigation}) => {
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [showModal, setShowModal] = useState(false);
  const dispatch = useDispatch();

  async function Regis() {
    try {
      dispatch(Register(username, email, password));
      navigation.navigate('Login');
    } catch (e) {
      console.log('errors', e);
    }
  }

  const state = useSelector(state => state.Auth);
  // console.log(state, 'Registrasi Berhasil');

  const registrasi = () => {
    console.log('Berhasil Registrasi');
    setShowModal(true);
  };

  return (
    <View style={styles.container}>
      <ImageBackground
        source={icBackground}
        style={{
          flex: 1,
          resizeMode: 'cover',
          justifyContent: 'center',
          // alignContent: 'center',
        }}>
        <View style={styles.Isi}>
          <Text style={styles.title}>Silahkan Registrasi Terlebih Dahulu</Text>
          <TextInput
            style={styles.input}
            placeholder="Username"
            value={username}
            onChangeText={setUsername}
          />
          <TextInput
            style={styles.input}
            placeholder="Email"
            value={email}
            onChangeText={setEmail}
          />
          <TextInput
            style={styles.input}
            placeholder="Password"
            secureTextEntry
            value={password}
            onChangeText={setPassword}
          />
          <View>
            <TouchableOpacity
              style={styles.button}
              onPress={() => {
                registrasi();
              }}>
              <Text style={styles.buttonText}>Registrasi</Text>
            </TouchableOpacity>
          </View>
          <Modal
            animationType="fade"
            transparent={false}
            visible={showModal}
            onRequestClose={() => setShowModal(false)}>
            <View
              style={{
                flex: 1,
                backgroundColor: 'white',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <View
                style={{
                  backgroundColor: '#2a364e',
                  padding: 30,
                  borderRadius: 30,
                }}>
                <Text
                  style={{
                    fontSize: 30,
                  }}></Text>
                <Text
                  style={{
                    color: 'white',
                  }}>
                  Selamat Anda Berhasil Registrasi
                </Text>
                <TouchableOpacity onPress={() => setShowModal(false)}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}>
                    <Text
                      style={{
                        fontWeight: 'bold',
                        color: 'white',
                        // marginTop: 10,
                        // textAlign: 'center',
                        // TextAlign: 'center',
                        fontSize: 20,
                        right: -90,
                        marginTop: 10,
                        // justifyContent: 'center',
                      }}
                      onPress={() => {
                        Regis();
                      }}>
                      Ok
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>
        </View>
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  Isi: {
    margin: 40,
    justifyContent: 'center',
    alignContent: 'center',
  },
  title: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 30,
    alignSelf: 'center',
    marginTop: 150,
  },
  input: {
    // width: '80%',
    height: 40,
    justifyContent: 'center',
    alignContent: 'center',
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 5,
    paddingHorizontal: 10,
    marginBottom: 20,
  },
  button: {
    backgroundColor: '#2a364e',
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 5,
  },
  buttonText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
    alignSelf: 'center',
  },
  button1: {
    fontSize: 12,
    padding: 15,
    alignSelf: 'center',
  },
});

export default Registrasi;
