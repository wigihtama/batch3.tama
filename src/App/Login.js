import React, {useState} from 'react';
import {
  Image,
  ImageBackground,
  StyleSheet,
  Text,
  TextInput,
  Modal,
  TouchableOpacity,
  View,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
// import icLogo from '../icons/segitiga1.png';
import icBackground from '../icons/4.png';
import {getData} from '../redux/action/Action';

const Login = ({navigation}) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [showModal, setShowModal] = useState(false);

  const dispatch = useDispatch();

  const state = useSelector(state => state.Auth.DataAuth);
  console.log(state, 'Data');

  const validateEmail = () => {
    if (username == 'Tam') {
      console.log(username);
      return true;
    } else {
      // console;
      if (username === '') {
        console.log('Error', 'Username tidak boleh kosong');
        return false;
      }

      if (username !== setUsername) {
        console.error('Error', 'Username Failed');
        return false;
      }
    }
    return false;
  };

  const validatePassword = () => {
    if (password == '123') {
      console.log(password);
      return true;
    } else {
      // console;
      if (password === '') {
        console.log('Error', 'Password tidak boleh kosong');
        return false;
      }
      if (password !== setPassword) {
        console.error('Eror', 'Password Failed');
        return false;
      }
    }
    return false;
  };

  async function Logino() {
    try {
      dispatch(getData(username, password));
    } catch (e) {
      console.log('errors', e);
    }
  }

  const login = () => {
    if (validateEmail() && validatePassword()) {
      console.log('Berhasil Login');
      setShowModal(true);
    }
  };

  return (
    <View style={styles.container}>
      <ImageBackground
        source={icBackground}
        style={{
          flex: 1,
          resizeMode: 'cover',
          justifyContent: 'center',
          // margin:  Platform.OS == 'android' ? 70 : 345,
          // alignContent: 'center',
        }}>
        <View style={styles.Isi}>
          {/* <Image
            source={icLogo}
            style={{
              width: 150,
              height: 100,
              marginTop: -30,
              resizeMode: 'contain',
              alignSelf: 'center',
            }} */}

          <Text style={styles.title}>Login</Text>
          <TextInput
            style={styles.input}
            placeholder="Username"
            value={username}
            onChangeText={setUsername}
          />
          <TextInput
            style={styles.input}
            placeholder="Password"
            secureTextEntry
            value={password}
            onChangeText={setPassword}
          />
          <View>
            <TouchableOpacity
              style={styles.button}
              onPress={() => {
                login();
              }}>
              <Text style={styles.buttonText}>Login</Text>
            </TouchableOpacity>
          </View>

          <Modal
            animationType="slide"
            transparent={false}
            visible={showModal}
            onRequestClose={() => setShowModal(false)}>
            <View
              style={{
                flex: 1,
                backgroundColor: 'white',
                alignItems: 'center',
                justifyContent: 'center',
              }}>
              <View
                style={{
                  backgroundColor: '#2a364e',
                  padding: 30,
                  borderRadius: 30,
                }}>
                <Text
                  style={{
                    fontSize: 30,
                    color: 'white',
                  }}></Text>
                <Text
                  style={{
                    color: 'white',
                  }}>
                  Selamat Anda Berhasil Login
                </Text>
                <TouchableOpacity onPress={() => setShowModal(false)}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}>
                    <Text
                      style={{
                        fontWeight: 'bold',
                        color: 'white',
                        // marginTop: 10,
                        // textAlign: 'center',
                        // TextAlign: 'center',
                        fontSize: 20,
                        right: -73,
                        marginTop: 10,
                        // justifyContent: 'center',
                      }}
                      onPress={() => {
                        Logino();
                      }}>
                      Ok
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>
          <Text style={styles.button1}>Belum Punya Akun?</Text>

          <TouchableOpacity
            onPress={() => {
              navigation.navigate('Registrasi');
            }}>
            <Text
              style={{
                alignSelf: 'center',
              }}>
              Daftar
            </Text>
          </TouchableOpacity>
        </View>
      </ImageBackground>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  Isi: {
    margin: 40,
    justifyContent: 'center',
    alignContent: 'center',
  },
  title: {
    fontSize: 30,
    fontWeight: 'bold',
    marginBottom: 30,
    alignSelf: 'center',
    marginTop: 150,
  },
  input: {
    // width: '80%',
    height: 40,
    justifyContent: 'center',
    alignContent: 'center',
    borderColor: 'gray',
    borderWidth: 1,
    borderRadius: 5,
    paddingHorizontal: 10,
    marginBottom: 20,
  },
  button: {
    backgroundColor: '#2a364e',
    paddingHorizontal: 20,
    paddingVertical: 10,
    borderRadius: 5,
  },
  buttonText: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
    alignSelf: 'center',
  },
  button1: {
    fontSize: 12,
    padding: 15,
    alignSelf: 'center',
  },
});

export default Login;
