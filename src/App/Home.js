import React, {useEffect, useState} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {
  View,
  StyleSheet,
  SafeAreaView,
  Image,
  Platform,
  ScrollView,
  Text,
  Modal,
  TextInput,
  TouchableOpacity,
} from 'react-native';

import icPulsa from '../icons/hp1.png';
import icTransfer from '../icons/transfer2.png';
import icWallet from '../icons/wallet3.png';
import icCard from '../icons/card2.png';
import icTarik from '../icons/tarik1.png';
import icCard1 from '../icons/qris.png';
import icListrik from '../icons/listrik2.png';
import icLain from '../icons/lain2.png';
import icOvo from '../icons/ovo.jpeg';
import icDana from '../icons/dana.jpeg';
import icIklan from '../icons/iklan.jpeg';
import icIklan2 from '../icons/iklan2.jpeg';
import icIklan3 from '../icons/iklan3.jpeg';
import icIklan4 from '../icons/iklan4.jpeg';
import icIklan5 from '../icons/iklan5.jpeg';
import icFood1 from '../icons/telkomsel.jpeg';
import icFood2 from '../icons/indosat.jpeg';
import icFood3 from '../icons/axis.jpeg';
import icFood4 from '../icons/tri.jpeg';
import icFood5 from '../icons/xl.jpeg';
import icLogout from '../icons/logout1.png';
import {useDispatch, useSelector} from 'react-redux';
import {Getone, Logouts} from '../redux/action/Action';
import AsyncStorage from '@react-native-async-storage/async-storage';

// const Stack = createNativeStackNavigator();
// const Tab = createBottomTabNavigator();

const Home = ({navigation}) => {
  const [showModal, setShowModal] = useState(false);
  // const [nama, setNama] = useState('ISDU');
  // // cara kirim data ke action

  // const dispatch = useDispatch();
  // const GetAction = async () => {
  //   try {
  //     dispatch(Getone(nama));
  //   } catch (e) {
  //     console.log(e);
  //   }
  // };

  // useEffect(() => {
  //   GetAction();
  // }, []);

  // cara ambil data dari reducer adalah
  // const state = useSelector(state => state.Auth);
  // console.log('ini auth lo', state);

  const dispatch = useDispatch();
  async function logout() {
    try {
      console.log('Berhasil Logout');
      dispatch(Logouts());
      await AsyncStorage.removeItem('Token');
    } catch (e) {
      console.log('error', e);
    }
  }
  const logouts = () => {
    console.log('Berhasil Logout');
    setShowModal(true);
  };

  return (
    <SafeAreaView style={styles.container}>
      <View>
        <View>
          <TouchableOpacity
            onPress={() => {
              logouts();
            }}>
            <Image
              source={icLogout}
              style={{
                margin: 10,
                width: 34,
                height: 34,
                resizeMode: 'contain',
                marginLeft: Platform.OS == 'android' ? 370 : 345,
              }}
            />
          </TouchableOpacity>
        </View>
        <Modal
          animationType="fade"
          transparent={false}
          visible={showModal}
          onRequestClose={() => setShowModal(false)}>
          <View
            style={{
              flex: 1,
              backgroundColor: 'white',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <View
              style={{
                backgroundColor: '#2a364e',
                padding: 30,
                borderRadius: 30,
              }}>
              <Text
                style={{
                  fontSize: 30,
                }}></Text>
              <Text
                style={{
                  color: 'white',
                }}>
                Anda Telah Keluar
              </Text>
              <TouchableOpacity onPress={() => setShowModal(false)}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <Text
                    style={{
                      fontWeight: 'bold',
                      color: 'white',
                      // marginTop: 10,
                      // textAlign: 'center',
                      // TextAlign: 'center',
                      fontSize: 20,
                      right: -40,
                      marginTop: 10,
                      // justifyContent: 'center',
                    }}
                    onPress={() => {
                      logout();
                    }}>
                    Ok
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
        <Text style={styles.header}>Saldo Anda</Text>
        <Text style={styles.header}>Rp 26.897.327,12</Text>
      </View>
      <View>
        <View style={styles.box}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}></View>
        </View>
      </View>

      <ScrollView>
        <View>
          {/* <View
            style={{
              borderWidth: 0.2,
              margin: 8,
              borderRadius: 10,
              shadowColor: 'black',
              shadowOffset: {width: 10, height: 10},
              shadowOpacity: 90,
              shadowRadius: 300,
              elevation: 30,
              backgroundColor: '#2a364e',
            }}> */}
          {/* MENU */}
          {/* <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}> */}
          {/* MENU 1 */}
          <View
            style={{
              // flex:1,
              backgroundColor: 'white',
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <View>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('Trx');
                }}>
                <View style={styles.iniIcon}>
                  <Image
                    source={icTransfer}
                    style={{
                      width: 34,
                      height: 34,
                      resizeMode: 'contain',
                      alignSelf: 'center',
                    }}
                  />
                </View>
              </TouchableOpacity>
              <Text
                style={{
                  textAlign: 'center',
                }}>
                Transfer
              </Text>
            </View>
            {/* MENU 2 */}
            <View>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('Puls');
                }}>
                <View style={styles.iniIcon}>
                  <Image
                    source={icPulsa}
                    style={{
                      width: 34,
                      height: 34,
                      resizeMode: 'contain',
                      alignSelf: 'center',
                    }}
                  />
                </View>
              </TouchableOpacity>

              <Text
                style={{
                  textAlign: 'center',
                }}>
                Pulsa
              </Text>
            </View>
            <View>
              <View style={styles.iniIcon}>
                <Image
                  source={icWallet}
                  style={{
                    width: 37,
                    height: 37,
                    resizeMode: 'contain',
                    alignSelf: 'center',
                  }}
                />
              </View>
              <View
                style={{
                  flexDirection: 'column',
                }}>
                <Text
                  style={{
                    textAlign: 'center',
                  }}>
                  Dompet
                </Text>
                <Text
                  style={{
                    textAlign: 'center',
                  }}>
                  Digital
                </Text>
              </View>
            </View>
            <View>
              <View style={styles.iniIcon}>
                <Image
                  source={icCard}
                  style={{
                    width: 34,
                    height: 34,
                    resizeMode: 'contain',
                    alignSelf: 'center',
                  }}
                />
              </View>
              <Text
                style={{
                  textAlign: 'center',
                }}>
                My Card
              </Text>
            </View>
          </View>
          <View
            style={{
              // flex:1,
              backgroundColor: 'white',
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <View>
              <View style={styles.iniIcon}>
                <Image
                  source={icTarik}
                  style={{
                    width: 34,
                    height: 34,
                    resizeMode: 'contain',
                    alignSelf: 'center',
                  }}
                />
              </View>
              <Text
                style={{
                  textAlign: 'center',
                }}>
                Tarik Tunai
              </Text>
            </View>
            <View>
              <View style={styles.iniIcon}>
                <Image
                  source={icCard1}
                  style={{
                    width: 34,
                    height: 34,
                    resizeMode: 'contain',
                    alignSelf: 'center',
                  }}
                />
              </View>
              <Text
                style={{
                  textAlign: 'center',
                }}>
                QRIS
              </Text>
            </View>

            <View>
              <View style={styles.iniIcon}>
                <Image
                  source={icListrik}
                  style={{
                    width: 34,
                    height: 34,
                    resizeMode: 'contain',
                    alignSelf: 'center',
                  }}
                />
              </View>
              <Text
                style={{
                  textAlign: 'center',
                }}>
                Listrik
              </Text>
            </View>

            <View>
              <View style={styles.iniIcon}>
                <Image
                  source={icLain}
                  style={{
                    width: 34,
                    height: 34,
                    resizeMode: 'contain',
                    alignSelf: 'center',
                  }}
                />
              </View>
              <Text
                style={{
                  textAlign: 'center',
                }}>
                Lainnya
              </Text>
            </View>
          </View>
          {/* </View> */}
          {/* </ScrollView> */}
          {/* BALANCE */}
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              backgroundColor: 'white',
            }}>
            <View style={styles.balance}>
              <View
                style={{
                  justifyContent: 'center',
                  alignContent: 'center',
                }}>
                <Text style={{color: 'black'}}>Saldo Ovo</Text>
                <Text style={{fontWeight: 'bold', color: 'black'}}>
                  Rp 19.823
                </Text>
              </View>
              <Image
                source={icOvo}
                style={{
                  width: 30,
                  height: 30,
                  resizeMode: 'contain',
                  alignSelf: 'center',
                }}
              />
            </View>

            <View style={styles.balance}>
              <View
                style={{
                  justifyContent: 'center',
                  alignContent: 'center',
                }}>
                <Text style={{color: 'black'}}>Saldo Dana</Text>
                <Text style={{fontWeight: 'bold', color: 'black'}}>
                  Rp 8.815
                </Text>
              </View>
              <Image
                source={icDana}
                style={{
                  width: 30,
                  height: 30,
                  resizeMode: 'contain',
                  alignSelf: 'center',
                }}
              />
            </View>
          </View>
          {/* PESAN SEKARANG */}
          <View
            style={{
              backgroundColor: 'white',
            }}>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}>
              <View
                style={{
                  flexDirection: 'row',
                }}>
                <View
                  style={{
                    height: Platform.OS == 'android' ? 250 : 280,
                    width: Platform.OS == 'android' ? 390 : 370,

                    backgroundColor: '#white',

                    borderRadius: 10,
                    margin: 10,
                    justifyContent: 'center',
                    alignContent: 'center',
                  }}>
                  <Image
                    source={icIklan}
                    style={{
                      height: 240,
                      width: Platform.OS == 'android' ? 380 : 360,
                    }}
                  />
                </View>

                <View
                  style={{
                    height: 250,
                    width: Platform.OS == 'android' ? 390 : 370,

                    backgroundColor: 'white',

                    borderRadius: 10,
                    margin: 10,
                    justifyContent: 'center',
                    alignContent: 'center',
                  }}>
                  <Image
                    source={icIklan5}
                    style={{
                      height: 240,
                      width: Platform.OS == 'android' ? 380 : 360,
                    }}
                  />
                </View>

                <View
                  style={{
                    height: 250,
                    width: Platform.OS == 'android' ? 390 : 370,

                    backgroundColor: 'white',

                    borderRadius: 10,
                    margin: 10,
                    justifyContent: 'center',
                    alignContent: 'center',
                  }}>
                  <Image
                    source={icIklan3}
                    style={{
                      height: 240,
                      width: Platform.OS == 'android' ? 380 : 360,
                    }}
                  />
                </View>
                <View
                  style={{
                    height: 250,
                    width: Platform.OS == 'android' ? 390 : 370,

                    backgroundColor: 'white',

                    borderRadius: 10,
                    margin: 10,
                    justifyContent: 'center',
                    alignContent: 'center',
                  }}>
                  <Image
                    source={icIklan4}
                    style={{
                      height: 240,
                      width: Platform.OS == 'android' ? 380 : 360,
                    }}
                  />
                </View>
                <View
                  style={{
                    height: 250,
                    width: Platform.OS == 'android' ? 390 : 370,

                    backgroundColor: 'white',

                    borderRadius: 10,
                    margin: 10,
                    justifyContent: 'center',
                    alignContent: 'center',
                  }}>
                  <Image
                    source={icIklan2}
                    style={{
                      height: 240,
                      width: Platform.OS == 'android' ? 380 : 360,
                    }}
                  />
                </View>
              </View>
            </ScrollView>
            <Text
              style={{
                fontWeight: 'bold',
                fontSize: 16,
                marginLeft: 10,
              }}>
              Transaksi Sekarang & Nikmati Diskonnya
            </Text>
            <Text
              style={{
                marginLeft: 10,
              }}>
              Sponsored by Apa aja boleh
            </Text>

            <View>
              <Text
                style={{
                  fontWeight: 'bold',
                  marginLeft: 10,
                  marginTop: Platform.OS == 'android' ? 30 : 40,
                  fontSize: 15,
                }}>
                Pilih yang mana provider kamu
              </Text>
              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginTop: 10,
                    marginBottom: 100,
                  }}>
                  <View
                    style={{
                      height: 100,
                      width: Platform.OS == 'android' ? 140 : 120,
                      backgroundColor: 'white',
                      marginLeft: 5,
                      justifyContent: 'center',
                      alignContent: 'center',
                    }}>
                    <Image
                      source={icFood1}
                      style={{
                        height: 100,
                        width: Platform.OS == 'android' ? 130 : 110,
                      }}
                    />
                  </View>

                  <View
                    style={{
                      height: 100,
                      width: Platform.OS == 'android' ? 140 : 120,
                      marginLeft: 5,
                      backgroundColor: 'white',
                      justifyContent: 'center',
                      alignContent: 'center',
                    }}>
                    <Image
                      source={icFood2}
                      style={{
                        height: 100,
                        width: Platform.OS == 'android' ? 130 : 110,
                      }}
                    />
                  </View>
                  <View
                    style={{
                      height: 100,
                      width: Platform.OS == 'android' ? 140 : 120,
                      marginLeft: 5,
                      backgroundColor: 'white',
                      justifyContent: 'center',
                      alignContent: 'center',
                    }}>
                    <Image
                      source={icFood3}
                      style={{
                        height: 100,
                        width: Platform.OS == 'android' ? 130 : 110,
                      }}
                    />
                  </View>
                  <View
                    style={{
                      height: 100,
                      width: Platform.OS == 'android' ? 140 : 120,
                      marginLeft: 5,
                      backgroundColor: 'white',
                      justifyContent: 'center',
                      alignContent: 'center',
                    }}>
                    <Image
                      source={icFood4}
                      style={{
                        height: 100,
                        width: Platform.OS == 'android' ? 130 : 110,
                      }}
                    />
                  </View>
                  <View
                    style={{
                      height: 100,
                      width: Platform.OS == 'android' ? 140 : 120,
                      marginLeft: 5,
                      backgroundColor: 'white',
                      justifyContent: 'center',
                      alignContent: 'center',
                    }}>
                    <Image
                      source={icFood5}
                      style={{
                        height: 100,
                        width: Platform.OS == 'android' ? 130 : 110,
                      }}
                    />
                  </View>
                </View>
              </ScrollView>
            </View>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#2a364e',
    // alignContent: 'center',
    // justifyContent:'center',
    // alignItems:'center'
  },
  header: {
    textAlign: 'center',
    fontSize: 20,
    color: 'white',
    // top: 0,
  },
  box: {
    width: 0.1,
    // height: 5,
    backgroundColor: 'white',
    top: 50,
    bottom: 50,
    marginBottom: 20,
  },

  iniIcon: {
    flexDirection: 'row',
    marginTop: 20,
    width: 50,
    height: 50,
    backgroundColor: '#2a364e',
    borderColor: '#66CCFF',
    borderWidth: 1,
    justifyContent: 'center',
    alignContent: 'center',
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 100,
    shadowColor: 'grey',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 9,
    shadowRadius: 3,
    elevation: 30,
  },
  balance: {
    marginTop: 20,
    width: Platform.OS == 'android' ? 185 : 175,
    height: Platform.OS == 'android' ? 50 : 60,
    borderWidth: 1,
    backgroundColor: '#e6e6e6',
    borderColor: 'black',
    // borderWidth:0.3,
    justifyContent: 'space-between',
    alignContent: 'center',
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 7,
    flexDirection: 'row',
    padding: 10,
  },
});

export default Home;
