import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  Platform,
  TextInput,
  KeyboardAvoidingView,
  ScrollView,
  Modal,
  BackHandler,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import axios from 'axios';
import {Transfer} from '../redux/action/Action';
import {useDispatch, useSelector} from 'react-redux';

const Trx = ({navigation}) => {
  const [dataSender, setSender] = useState('');
  const [dataTarget, setTarget] = useState('');
  const [dataAmount, setAmount] = useState('');
  const [dataType, setType] = useState('');
  const [showModal, setShowModal] = useState(false);
  const [status, setStatus] = useState('');
  const dispatch = useDispatch();

  const HandlerBack = () => {
    navigation.goBack();
    return true;
  };

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', HandlerBack);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', HandlerBack);
    };
  }, []);

  const state = useSelector(state => state.Fetchs.Transfer);

  async function Transf() {
    dispatch(Transfer(dataAmount, dataSender, dataTarget, dataType));
  }

  function sendTransfer() {
    Transf(dataAmount, dataSender, dataTarget, dataType);
  }

  const handlePress = () => {
    sendTransfer();
    setShowModal(true);
  };

  return (
    <KeyboardAvoidingView style={{flex: 1}}>
      <SafeAreaView style={styles.canvas.container}>
        <ScrollView>
          <View style={styles.canvas.box}>
            <View style={styles.canvas.baratas}>
              <View style={styles.canvas.bartransfer}>
                <Text style={styles.text.mtransfer}>Transaksi</Text>
              </View>
            </View>
            {/* // Penutup bar tengah */}
            <View>
              <View style={styles.canvas.boxtengahtransfer}>
                {/* // Penutup text bar */}
                <View>
                  <TextInput
                    placeholder="Nama Penerima"
                    keyboardType="default"
                    placeholderTextColor={'grey'}
                    // fontFamily="Poppins-Regular"
                    style={styles.text.sender}
                    onChangeText={sender => {
                      setSender(sender);
                    }}
                  />
                </View>
                <View>
                  <TextInput
                    placeholder="No. Rekening Penerima"
                    keyboardType="numeric"
                    placeholderTextColor={'grey'}
                    // fontFamily="Poppins-Regular"
                    style={styles.text.receiver}
                    onChangeText={receiver => {
                      setTarget(receiver);
                    }}
                  />
                </View>
                <View>
                  <TextInput
                    placeholder="Jumlah"
                    keyboardType="numeric"
                    placeholderTextColor={'grey'}
                    // fontFamily="Poppins-Regular"
                    style={styles.text.amount}
                    onChangeText={amount => {
                      setAmount(amount);
                    }}
                  />
                </View>
                <View>
                  <TextInput
                    placeholder="Catatan"
                    keyboardType="default"
                    placeholderTextColor={'grey'}
                    // fontFamily="Poppins-Regular"
                    style={styles.text.status}
                    onChangeText={status => {
                      setType(status);
                    }}
                  />
                </View>

                <View>
                  <TouchableOpacity
                    onPress={() => {
                      handlePress();
                    }}>
                    <View style={styles.canvas.buttonsend}>
                      <Text style={styles.text.send}>Transfer</Text>
                    </View>
                  </TouchableOpacity>
                  <Modal
                    animationType="fade"
                    transparent={false}
                    visible={showModal}
                    onRequestClose={() => setShowModal(false)}>
                    <View
                      style={{
                        flex: 1,
                        backgroundColor: 'white',
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      <View
                        style={{
                          backgroundColor: '#2a364e',
                          padding: 100,
                          borderRadius: 30,
                        }}>
                        <Text
                          style={{
                            fontSize: 28,
                            fontWeight: 'bold',
                            marginBottom: 10,
                            textAlign: 'center',
                            color: 'white',
                          }}>
                          Status
                        </Text>
                        <Text
                          style={{
                            fontSize: 20,
                            color: 'white',
                          }}>
                          {status}
                        </Text>
                        <Text
                          style={{
                            color: 'white',
                            margin: 5,
                          }}>
                          Nama Penerima : {dataSender}
                        </Text>
                        <Text
                          style={{
                            color: 'white',
                            margin: 5,
                          }}>
                          No. Rek : {dataTarget}
                        </Text>
                        <Text
                          style={{
                            color: 'white',
                            margin: 5,
                          }}>
                          Jumlah Total : Rp. {dataAmount}
                        </Text>
                        <Text
                          style={{
                            color: 'white',
                            margin: 5,
                          }}>
                          Catatan : {dataType}
                        </Text>
                        <TouchableOpacity onPress={() => setShowModal(false)}>
                          <View
                            style={{
                              flexDirection: 'row',
                              justifyContent: 'space-between',
                            }}>
                            <Text
                              style={{
                                fontWeight: 'bold',
                                color: 'white',
                                marginTop: 10,
                                top: 70,
                                // textAlign: 'center',
                                left: -60,
                                fontSize: 20,
                                // marginRight:10
                              }}>
                              Close
                            </Text>
                            <Text
                              style={{
                                fontWeight: 'bold',
                                color: 'white',
                                marginTop: 10,
                                top: 70,
                                // TextAlign: 'center',
                                fontSize: 20,
                                right: -60,
                              }}
                              onPress={() => {
                                navigation.navigate('History');
                              }}>
                              Ke History
                            </Text>
                          </View>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </Modal>
                </View>
              </View>
            </View>
            {/* // batas penutup view bar tengah */}
          </View>
        </ScrollView>
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  canvas: {
    container: {backgroundColor: 'white', flex: 1},

    box: {
      backgroundColor: '#2a364e',
      height: Platform.OS == 'android' ? 580 : 690,

      borderRadius: 25,
      marginLeft: 10,
      marginRight: 10,
      marginTop: 10,
    },
    baratas: {
      backgroundColor: 'white',
      width: 360,
      height: 50,
      marginLeft: Platform.OS == 'android' ? 13 : 4,
      borderRadius: 25,
      alignItems: 'center',
      justifyContent: 'center',
      marginTop: 20,
    },
    bartransfer: {
      backgroundColor: 'white',
      height: 30,
      width: 340,
      borderRadius: 10,
    },
    boxtengahtransfer: {
      backgroundColor: 'white',
      width: Platform.OS == 'ios' ? 350 : 370,
      height: Platform.OS == 'ios' ? 350 : 380,
      borderRadius: 10,
      marginTop: 60,
      marginLeft: 11,
    },
    buttonsend: {
      backgroundColor: '#5973a6',
      width: 200,
      height: 70,
      marginTop: 20,
      marginLeft: 70,
      borderRadius: 45,
    },
  },
  text: {
    mtransfer: {
      color: 'black',
      fontSize: 17,
      textAlign: 'center',
      // fontFamily: 'Poppins-Regular',
      marginTop: Platform.OS == 'ios' ? 3 : 3,
    },

    sender: {
      borderWidth: 1,
      borderColor: 'black',
      borderRadius: 20,
      fontSize: 18,
      marginTop: Platform.OS == 'android' ? 30 : 35,
      padding: 10,
      marginLeft: 10,
      marginRight: 10,
      color: 'black',
    },

    receiver: {
      borderWidth: 1,
      borderColor: 'black',
      borderRadius: 20,
      fontSize: 18,
      marginTop: 30,
      padding: 10,
      marginLeft: 10,
      marginRight: 10,
      color: 'black',
    },
    amount: {
      borderWidth: 1,
      borderColor: 'black',
      borderRadius: 20,
      fontSize: 18,
      padding: 10,
      marginTop: 30,
      marginLeft: 10,
      marginRight: 10,
      color: 'black',
    },
    status: {
      borderColor: 'black',
      borderWidth: 1,
      borderRadius: 20,
      fontSize: 18,
      padding: 10,
      marginTop: 30,
      marginLeft: 10,
      marginRight: 10,
      color: 'black',
    },
    send: {
      color: 'white',
      marginTop: 18,
      fontSize: 25,
      // fontFamily: 'Poppins-Regular',
      textAlign: 'center',
    },
  },
});
export default Trx;
