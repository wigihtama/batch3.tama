import {
  View,
  Text,
  BackHandler,
  StyleSheet,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
  Image,
  TextInput,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import axios from 'axios';
// import {RadioButton, TextInput} from 'react-native-paper';
// import Telpone from '../../Image/Telpon.png';
// import Bank from '../../Image/Bank.png';
// import Coni from '../../Image/Icon.png';

const Transaksi = ({navigation}) => {
  const HandlerBack = () => {
    navigation.goBack();
    return true;
  };

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', HandlerBack);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', HandlerBack);
    };
  }, []);

  const [disables, setDisables] = useState(false);
  const [dataAmount, setDataAmount] = useState('');
  const [dataSender, setDataSender] = useState('');
  const [dataTarget, setDataTarget] = useState('');
  const [dataType, setDataType] = useState('');
  const [showModal, setShowModal] = useState(false);

  // useEffect(() => {
  //   ValidatorButton();
  // }, [dataAmount, dataTarget, dataType]);

  const rupiah = x => {
    return x
      ?.toString()
      .replace(/\./g, '')
      .replace(/(\d)(?=(\d{3})+$)/g, '$1' + '.');
  };

  function tambah() {
    // var axios = require('axios');
    var data = JSON.stringify({
      amount: dataAmount,
      sender: dataSender,
      target: dataTarget,
      type: dataType,
    });

    var config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: 'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json',
      headers: {
        'Content-Type': 'application/json',
      },
      data: data,
    };

    axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data));
      })
      .catch(function (error) {
        console.log(error);
      });
  }
  const handlePress = () => {
    setShowModal(true);
  };
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : undefined}
      style={{flex: 1}}>
      <SafeAreaView style={style.container}>
        <ScrollView>
          <View>
            <View>
              {/* <Image
                source={Coni}
                style={{height: 35, width: 40, resizeMode: 'center'}}
              /> */}
              <Text
                style={{
                  marginTop: 15,
                  marginBottom: 15,
                  textAlign: 'center',
                  fontWeight: 'bold',
                  color: 'black',
                  fontSize: 25,
                }}>
                Transaksi
              </Text>
            </View>

            <View>
              <Text
                style={{
                  color: 'black',
                  marginLeft: 30,
                  fontSize: 16,
                }}>
                Transfer
              </Text>
              {/* <View
                style={{
                  flexDirection: 'column',
                  margin: 5,
                }}>
                <View
                  style={{
                    marginTop: 10,
                    marginLeft: 15,
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <RadioButton
                    value="kontak"
                    status={dataType === 'kontak' ? 'checked' : 'unchecked'}
                    onPress={() => setDataType('kontak')}
                  />
                  <Image
                    source={Telpone}
                    style={{height: 30, width: 40, resizeMode: 'center'}}
                  />
                  <Text
                    style={{
                      color: 'black',
                      fontSize: 16,
                      fontWeight: 'bold',
                      marginLeft: 10,
                    }}>
                    Antar No. Hp
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    marginTop: 10,
                    marginLeft: 15,
                    alignItems: 'center',
                  }}>
                  <RadioButton
                    value="bank"
                    status={dataType === 'bank' ? 'checked' : 'unchecked'}
                    onPress={() => setDataType('bank')}
                  />
                  <Image
                    source={Bank}
                    style={{height: 35, width: 40, resizeMode: 'center'}}
                  />
                  <Text
                    style={{
                      color: 'black',
                      fontSize: 17,
                      fontWeight: 'bold',
                      marginLeft: 10,
                    }}>
                    Antara Bank
                  </Text>
                </View>
              </View> */}
            </View>

            <View style={{marginVertical: 15}}>
              <TextInput
                placeholder={'Nominal'}
                values={dataAmount}
                onChangeText={am => {
                  setDataAmount(rupiah(am));
                }}
                style={{
                  height: 50,
                  borderWidth: 1.5,
                  borderRadius: 5,
                  marginHorizontal: 20,
                  paddingHorizontal: 10,
                  backgroundColor: '#e6e6e6',
                }}
              />
            </View>

            <View style={{marginVertical: 5}}>
              <TextInput
                disabled={true}
                placeholder={'Pengirim'}
                values={dataSender}
                onChangeText={tg => {
                  setDataSender(tg);
                }}
                style={{
                  height: 50,
                  borderWidth: 1.5,
                  borderRadius: 5,
                  marginHorizontal: 20,
                  paddingHorizontal: 10,
                  backgroundColor: '#e6e6e6',
                }}
              />
            </View>

            <View style={{marginVertical: 5}}>
              <TextInput
                placeholder={'Penerima'}
                values={dataTarget}
                onChangeText={tg => {
                  setDataTarget(tg);
                }}
                style={{
                  height: 50,
                  borderWidth: 1.5,
                  borderRadius: 5,
                  marginHorizontal: 20,
                  paddingHorizontal: 10,
                  backgroundColor: '#e6e6e6',
                }}
              />
            </View>

            <View style={{marginVertical: 5}}>
              <TextInput
                placeholder={'Type'}
                values={dataType}
                onChangeText={tg => {
                  setDataType(tg);
                }}
                style={{
                  height: 50,
                  borderWidth: 1.5,
                  borderRadius: 5,
                  marginHorizontal: 20,
                  paddingHorizontal: 10,
                  backgroundColor: '#e6e6e6',
                }}
              />
            </View>

            <View>
              <TouchableOpacity
                disabled={disables}
                onPress={() => {
                  tambah();
                  navigation.navigate('History');
                }}
                style={{
                  backgroundColor: disables == false ? '#e6e6e6' : '#d9d9d9',
                  marginHorizontal: 70,
                  paddingVertical: 15,
                  marginTop: 25,
                  borderRadius: 20,
                  alignItems: 'center',
                }}>
                <Text
                  style={{
                    color: 'black',
                    fontSize: 20,
                    fontWeight: 'bold',
                  }}>
                  Bayar
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
};

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#66ccff',
  },
});

export default Transaksi;
