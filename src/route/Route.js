import {createNativeStackNavigator} from '@react-navigation/native-stack';
import BottomNav from '../Component/BottomNav';
import Trx from '../App/Trx';
import Registrasi from '../App/Registrasi';
import Login from '../App/Login';
import {useSelector} from 'react-redux';
import {useEffect, useState} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {NavigationContainer} from '@react-navigation/native';
import Puls from '../App/Pulsa';

const Stack = createNativeStackNavigator();
const AppStack = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="BottomNav" component={BottomNav} />
      <Stack.Screen name="Trx" component={Trx} />
      <Stack.Screen name="Puls" component={Puls} />
    </Stack.Navigator>
  );
};

const AuthStack = () => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="Registrasi" component={Registrasi} />
    </Stack.Navigator>
  );
};

function Router() {
  const [tokens, setTokens] = useState(null);
  const UpdateData = useSelector(state => state.Auth.DataAuth);

  const DataUser = async () => {
    const token = await AsyncStorage.getItem('Token');
    setTokens(JSON.parse(token));
  };
  useEffect(() => {
    DataUser();
  }, [UpdateData]);
  return (
    <NavigationContainer>
      {tokens !== null ? <AppStack /> : <AuthStack />}
    </NavigationContainer>
  );
}

export default Router;
