import {View, Text, Image} from 'react-native';
import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Home from '../App/Home';
import History from '../App/History';
import Registrasi from '../App/Registrasi';
import Login from '../App/Login';

const Tab = createBottomTabNavigator();
const BottomNav = () => {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarShowLabel: false, //untuk menghilangkan label di bottom tab biar bisa styling sendiri text labelnya
      }}>
      {/* tab screen untuk ngasih alamat yang ada di bottom tab biar bisa dipakai */}
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarActiveTintColor: 'red',
          tabBarIcon: ({size, color}) => (
            <View>
              <Image
                source={{
                  uri: 'https://png.pngtree.com/png-vector/20191126/ourmid/pngtree-home-vector-icon-png-image_2036119.jpg',
                }}
                style={{resizeMode: 'contain', width: 20, height: 20}}
              />
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="History"
        component={History}
        options={{
          tabBarActiveTintColor: 'red',
          tabBarIcon: ({size, color}) => (
            <View>
              <Image
                source={{
                  uri: 'https://w7.pngwing.com/pngs/988/206/png-transparent-computer-icons-history-icon-design-time-angle-text-logo-thumbnail.png',
                }}
                style={{resizeMode: 'contain', width: 20, height: 20}}
              />
            </View>
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default BottomNav;
