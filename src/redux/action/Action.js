import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';

export const getData = (username, password) => {
  const dataToken = async value => {
    await AsyncStorage.setItem('Token', value);
  };

  return async dispatch => {
    const data = {
      username,
      password,
    };
    dispatch({
      type: 'LOGIN',
      data: data,
    });
    dataToken(JSON.stringify(data));
  };
};

export const Register = (username, email, password) => {
  return async dispatch => {
    var data = JSON.stringify({
      name: username,
      email: email,
      pass: password,
    });

    var config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: 'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/users.json',
      headers: {
        'Content-Type': 'application/json',
      },
      data: data,
    };

    axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data));
        const data = JSON.stringify(response.data);
        dispatch({
          type: 'REGISTRASI',
          data: data,
        });
      })
      .catch(function (error) {
        console.log(error);
      });
  };
};

export const DataHistory = () => {
  const dataToken = async value => {
    await AsyncStorage.setItem('Token', value);
  };
  return async dispatch => {
    var data = '';

    var config = {
      method: 'get',
      maxBodyLength: Infinity,
      url: 'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json',
      headers: {},
      data: data,
    };
    axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data));
        const data = response.data;
        dispatch({
          type: 'HISTORY',
          data: data,
        });
        dataToken(JSON.stringify(data));
      })
      .catch(function (error) {
        console.log(error);
      });
  };
};

export const Logouts = () => {
  return dispatch => {
    const data = '';
    dispatch({
      type: 'LOGOUT',
      data: data,
    });
  };
};

export const Transfer = (dataAmount, dataSender, dataTarget, dataType) => {
  const dataToken = async value => {
    await AsyncStorage.setItem('Token', value);
  };
  return async dispatch => {
    var data = JSON.stringify({
      amount: dataAmount,
      sender: dataSender,
      target: dataTarget,
      type: dataType,
    });

    var config = {
      method: 'post',
      url: 'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json',
      headers: {
        'content-type': 'application/json',
      },
      data: data,
    };

    axios(config)
      .then(function (response) {
        const data = JSON.stringify(response.data);
        console.log(data, 'berhasil');
        dispatch({
          type: 'TRANSFER',
          data: data,
        });
        dataToken(JSON.stringify(data));
      })
      .catch(function (error) {
        console.log(error);
      });
    return true;
  };
};

export const Pulsa = (dataAmount, dataHP, dataHarga, dataType) => {
  const dataToken = async value => {
    await AsyncStorage.setItem('Token', value);
  };
  return async dispatch => {
    var data = JSON.stringify({
      amount: dataAmount,
      hp: dataHP,
      harga: dataHarga,
      type: dataType,
    });

    var config = {
      method: 'post',
      url: 'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json',
      headers: {
        'content-type': 'application/json',
      },
      data: data,
    };

    axios(config)
      .then(function (response) {
        const data = JSON.stringify(response.data);
        console.log(data, 'berhasil');
        dispatch({
          type: 'PULSA',
          data: data,
        });
        dataToken(JSON.stringify(data));
      })
      .catch(function (error) {
        console.log(error);
      });
    return true;
  };
};
