import {createStore, applyMiddleware} from 'redux';
import rootReducer from './reduce/Index';
import thunk from 'redux-thunk';

const storeRedux = createStore(rootReducer, applyMiddleware(thunk));

export default storeRedux;
