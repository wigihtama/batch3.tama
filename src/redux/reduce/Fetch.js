const initialState = {
  DataFetchs: [],
  History: [],
  Transfer: [],
  Pulsa: [],
};

const Fetchs = (state = initialState, action) => {
  switch (action.type) {
    case 'Home':
      return {
        ...state,
        DataFetchs: action.data,
      };
    case 'HISTORY':
      return {
        ...state,
        History: action.data,
      };
    case 'TRANSFER':
      return {
        ...state,
        Transfer: action.data,
      };
    case 'PULSA':
      return {
        ...state,
        Pulsa: action.data,
      };
    default:
      return state;
  }
};

export default Fetchs;
