import {combineReducers} from 'redux';
import Auth from './Auth';
import Fetchs from './Fetch';

export default combineReducers({
  Auth,
  Fetchs,
});
