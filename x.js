import React, {useState} from 'react';
import {View, Text, StyleSheet} from 'react-native';

const App = () => {
  const [saldo, setSaldo] = useState(1000000);

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Saldo Anda</Text>
      <Text style={styles.saldo}>Rp {saldo}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F5FCFF',
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  saldo: {
    fontSize: 36,
    fontWeight: 'bold',
    color: '#008000',
  },
});

export default App;
