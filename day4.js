// import React, { useEffect, useState } from 'react';
// import { SafeAreaView, StyleSheet, Text, View, FlatList, ScrollView, Image } from 'react-native';

// const App = () => {
//     const [item, setItem] = useState(null) //ap.tsx td lihat tipe datanya
//     const dataHistory = async() => {
//         var myHeaders = new Headers();
//         myHeaders.append("Content-Type", "application/json");

//         var raw = '';

//         var requestOptions = {
//         method: 'GET',
//         headers: myHeaders,
//         body: raw,
//         redirect: 'follow'
//         };

//         fetch("https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json", requestOptions)
//         .then(response => response.text())
//         .then(result => {
//             const val = JSON.parse(result);
//             setItem(Object.entries(val));
//             // console.log(result);
//         })
//         .catch(error => console.log('error', error));
// }
//     useEffect(() => {
//         dataHistory()
//     }, [])

//     return (
//         <SafeAreaView style={styles.container}>
//             <View style={styles.header}>
//                 <Text style={styles.headerText}>Riwayat Pembelian</Text>
//             </View>
//             <ScrollView showsVerticalScrollIndicator={false}>
//                     <View style={styles.content}>
//                         {item?.map((e:any, index:any)=>{

//                             return(
//                                 <View
//                                 style={{
//                                     flexDirection : 'row',
//                                     borderWidth :1,
//                                     margin : 10,
//                                     borderRadius : 7,
//                                     padding :7,
//                                     backgroundColor : '#e6f2ff',
//                                     justifyContent : 'space-between',
//                                     borderColor:'black',
//                                     borderBottomWidth: 5,
//                                     borderRightWidth: 5,
//                                     shadowColor: 'black',
//                                     shadowOffset: { width: 0, height: 2 },
//                                     shadowOpacity: 0.9,
//                                     shadowRadius: 3,
//                                     elevation: 3,

//                                 }}
//                                 >

//                                     <View>
//                                         <Text style={{
//                                             fontFamily:'Mynerve-Regular'
//                                         }}>Pengirim: {e[0]}</Text>
//                                         <Text style={{
//                                             fontFamily:'Mynerve-Regular'
//                                         }}>Penerima: {e[1].target}</Text>
//                                         <Text style={{
//                                             fontFamily:'Mynerve-Regular'
//                                         }}>Metode Pembayaran: {e[1].type}</Text>
//                                         <Text style={{
//                                             fontFamily:'Mynerve-Regular'
//                                         }}>Jumlah: {e[1].amount}</Text>
//                                     </View>
//                                     <Image
//                                         source={{uri:'https://img.icons8.com/dotty/512/purchase-order.png'}}
//                                         style={{
//                                             width:50,
//                                             height:50,
//                                             resizeMode:'contain'

//                                         }}
//                                         />
//                                 </View>
//                             )
//                         })}
//                     </View>

//             </ScrollView>
//         </SafeAreaView>
//     );
// };

// const styles = StyleSheet.create({
//     container: {
//         flex: 1,
//         backgroundColor: '#0077b3',
//         },
//         header: {
//             height: 50,
//             justifyContent: 'center',
//             alignItems: 'center',
//             // borderBottomWidth: 1,
//             // borderBottomColor: '#ccc',
//             backgroundColor: '#00aaff',
//             borderRadius:50,
//         },
//         headerText: {
//             fontSize: 18,
//             // fontFamily: 'Mynerve-Regular',
//             fontStyle:'italic',
//             color:'white'

//         },

// });

// export default App;

// import React from 'react';
// import { View, StyleSheet, SafeAreaView, useColorScheme } from 'react-native';

// const App = () => {
//     const theme = useColorScheme();
//     console.log(theme, '');
//   return (

//     <SafeAreaView style={[styles.container,{backgroundColor:theme == 'dark' ? 'rgba(0,0,0,0.8)':'white'}]}>
//       <View style={styles.kotak}>
//         <View style={styles.lingkaran1}>
//         <View style={styles.potongan}></View>
//           <View style={styles.lingkaran2}/>

//           <View style={styles.lingkaran3}></View>

//           <View style={styles.potongankanan}/>
//           <View style={styles.potongankiri}></View>
//         </View>
//       </View>
//     </SafeAreaView>
//   );
// };

// const styles = StyleSheet.create({

//   container: {
//     flex: 1,
//     alignContent: 'center',
//     justifyContent:'center',
//     alignItems:'center'
//   },
//   kotak: {
//     width: 150,
//     height: 150,
//     backgroundColor: 'white',
//     justifyContent: 'center',
//     alignItems: 'center',
//     borderBottomColor: 5,
//     shadowColor: 'black',
//     shadowOffset: { width: 0, height: 2 },
//     shadowOpacity: 0.9,
//     shadowRadius: 3,
//     position:'absolute'

//   },
//   lingkaran1: {
//     width: 125,
//     height: 125,
//     borderRadius: 100,
//     backgroundColor: 'green',
//     justifyContent: 'center',
//     alignItems: 'center',
//     position: 'absolute'
//   },
//   lingkaran2: {
//     width: 85,
//     height: 85,
//     borderRadius: 75,
//     backgroundColor: 'white',
//     position: 'absolute'
//   },
//   lingkaran3: {
//     width: 40,
//     height: 40,
//     borderRadius: 40,
//     backgroundColor: 'green',
//     justifyContent: 'center',
//     position: 'absolute'
//   },
//   potongan: {
//     width: 25,
//     height: 25,
//     borderRadius: 0,
//     backgroundColor: 'white',
//     top:50,
//     // justifyContent: 'center',

//   },
//   potongankiri: {
//     width: 21.5,
//     height: 20.5,
//     borderRadius: 11,
//     backgroundColor: 'green',
//     top:102.45,
//     left:35.3,
//     position:'absolute'
//     // justifyContent: 'center',
//   },
//   potongankanan: {
//     width: 21.5,
//     height: 20.5,
//     borderRadius: 11,
//     backgroundColor: 'green',
//     top:102.45,
//     right:35.3,
//     position:'absolute'
// },
// });

// export default App;
