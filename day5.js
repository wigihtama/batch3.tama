// import {View, Text, TouchableOpacity, SafeAreaView} from 'react-native';
// import React, {useEffect, useState} from 'react';

// const App = () => {
//   // const [count, setCount] = useState(0); // setCount untuk inisiasi data yan disimpan (in), dan count untuk mengeluarkan datanya (out)
//   const [count, setCount] = useState(false);
//   const incre = () => {
//     //setCount(count + 1); // ini ketika pake operator tambah atau operator kali
//     setCount(!count); // ini ketika pake boolean untuk sebuah perubahan data dari false menjadi true atau sebaliknya
//   };

//   useEffect(() => {
//     console.log('nomor yang anda tekan', count);
//   }, [count]); // setiap kali count datanya berubah maka console.log diatas akan mengikuti ambil data count yang baru
//   return (
//     <SafeAreaView>
//       <View>
//         <View>{count ? <Text>xxxxxxx</Text> : <></>}</View>
//         <TouchableOpacity
//           onPress={() => {
//             incre();
//           }}
//           style={{
//             backgroundColor: 'blue',
//             paddingHorizontal: 30,
//             paddingVertical: 20,
//             justifyContent: 'center',
//             alignItems: 'center',

//           }}>
//           <Text style={{fontSize: 18, color: 'white', fontWeight: 'bold'}}>
//             INCREMENT
//           </Text>
//         </TouchableOpacity>
//       </View>
//     </SafeAreaView>
//   );
// };

// export default App;

// In App.js in a new project

// REACT NAVIGATION

import {View, Text, TouchableOpacity, Image} from 'react-native';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native'; //navigation container yaitu wadahnya navigasi
import {createNativeStackNavigator} from '@react-navigation/native-stack'; // untuk membuat screen bisa di navigasi
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

//mebuat stack Screen atau Stack Navigator

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const bottomNav = () => {
  return (
    <Tab.Navigator
      screenOptions={{
        tabBarShowLabel: false, //untuk menghilangkan label di bottom tab biar bisa styling sendiri text labelnya
      }}>
      {/* tab screen untuk ngasih alamat yang ada di bottom tab biar bisa dipakai */}
      <Tab.Screen
        name="Home"
        component={HomeScreen}
        options={{
          tabBarActiveTintColor: 'red',
          tabBarIcon: ({size, color}) => (
            <View>
              <Image
                source={{
                  uri: 'https://png.pngtree.com/png-vector/20191126/ourmid/pngtree-home-vector-icon-png-image_2036119.jpg',
                }}
                style={{resizeMode: 'contain', width: 50, height: 30}}
              />
              <Text>HOMES</Text>
            </View>
          ),
        }}
      />
      <Tab.Screen name="Kedua" component={Kedua} />
    </Tab.Navigator>
  );
};

function HomeScreen({navigation}) {
  return (
    <View>
      <Text>HOME SCREEN NIH</Text>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('Kedua'); // ikuti namanya seperti name stack.screen (Harus SAMA)
        }}>
        <Text>Pindah LUR</Text>
      </TouchableOpacity>
    </View>
  );
}
function Kedua({navigation}) {
  return (
    <View>
      <Text>APAA AJAA DAH</Text>
      <TouchableOpacity
        onPress={() => {
          //navigation.navigate('Home');
          navigation.goBack(); // goBack() yaitu sebuah function dari si navigation dimana dia simpan navigasi history dan kembali ke screen sebelumnya
        }}>
        <Text>BALIK LAGI</Text>
      </TouchableOpacity>
    </View>
  );
}

const App = () => {
  return (
    <NavigationContainer>
      {/* ini adalah wadah untuk navigasi */}
      <Stack.Navigator>
        {/* dan stack navigator untuk wadahnya atau untuk simpan data screen */}
        {/*<Stack.Screen name="Home" component={HomeScreen} /> */}
        {/* name itu nantinya dipakai untuk nama navigasi seperti navigation.navigate('Home') karena namanya Home */}
        {/* <Stack.Screen name="Beda Halaman" component={bedaHalaman} /> */}
        {/* Component yaitu untuk import screen atau untuk baca screenya jadi bisa di tampilin di container */}
        <Stack.Screen
          name="Bottom"
          component={bottomNav}
          options={{headerShown: false}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
