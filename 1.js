import {View, Text, SafeAreaView, TouchableOpacity} from 'react-native';
import React from 'react';

const App = () => {
  const ambil = () => {
    var myHeaders = new Headers(); //headers untuk API atau kirim data ke backend
    myHeaders.append('Content-Type', 'application/json');

    var raw = JSON.stringify({
      name: 'test21',
    });

    var requestOptions = {
      method: 'PATCH', // GET, POST, DELETE, PATCH, PUT, OPTIONS
      headers: myHeaders, //headers untuk API atau kirim data ke backend
      body: raw, // ISI DATA YANG AKAN DIKIRIM
      redirect: 'follow',
    };

    fetch(
      // fetch ini adalah sebuah function yang ada dari javascript untuk ambil data API atau data backend
      'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/users/-NMalhNh5LPYMCq7uuUh.json',
      requestOptions,
    )
      // blob
      // maksud dari then adalah ketika API sudah dijalan maka diambil datanya
      .then(response =>
        // kita convert ke text dulu
        response.text(),
      )
      .then(result =>
        // isi yang sudah di convert dari si text()
        console.log(result),
      )
      .catch(error => console.log('error', error));
  };
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: '#7FFFD4'}}>
      <TouchableOpacity
        onPress={() => {
          ambil();
        }}>
        <Text>hayoooo</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
};

export default App;
