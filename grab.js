import React from 'react';
import {
  View,
  StyleSheet,
  SafeAreaView,
  Image,
  Platform,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import icScan from '../icons/scan.png';
import icSearch from '../icons/search.png';
import icLove from '../icons/love.png';
import icFood from '../icons/food.png';
import icBike from '../icons/bike.png';
import icCar from '../icons/car.png';
import icBox from '../icons/box.png';
import icMart from '../icons/mart.png';
import icPulsa from '../icons/PULSA.png';
import icJastip from '../icons/jastip.png';
import icDokter from '../icons/dokter.png';
import icPaket from '../icons/paket.png';
import icPromo from '../icons/promo.png';
import icHotel from '../icons/hotel.png';
import icTagihan from '../icons/tagihan.png';
import icJasa from '../icons/jasa.png';
import icSewa from '../icons/rent.png';
import icGift from '../icons/gift.png';
import icAsuransi from '../icons/asuransi.png';
import icGame from '../icons/game.png';
import icPetualangan from '../icons/petualangan.png';
import icOther from '../icons/other.png';
import icBalance1 from '../icons/balance1.png';
import icPesan1 from '../icons/pesan1.png';
import icPesan2 from '../icons/pesan2.png';
import icFood1 from '../icons/food1.jpeg';
import icFood2 from '../icons/food2.jpeg';
import icFood3 from '../icons/food3.png';
import icFood4 from '../icons/food4.jpeg';

const Home = ({navigation}) => {
  return (
    <SafeAreaView style={styles.container}>
      <View>
        <View style={styles.box}>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <View style={styles.search}>
              <Image
                source={icSearch}
                style={{
                  width: 28,
                  height: 28,
                  resizeMode: 'contain',
                  marginLeft: -10,
                  marginTop: -8,
                }}
              />
              <TextInput
                placeholder="makan"
                style={{
                  alignContent: 'center',
                  marginLeft: 4,
                  height: 10,
                }}
              />
            </View>
            <View style={styles.scan}>
              <Image
                source={icScan}
                style={{
                  width: 30,
                  height: 30,
                  resizeMode: 'contain',
                  alignSelf: 'center',
                }}
              />
            </View>
          </View>
        </View>
      </View>

      <ScrollView>
        <View>
          {/* MENU */}
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            {/* MENU 1 */}
            <View
              style={{
                // flex:1,
                backgroundColor: 'white',
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <View>
                <TouchableOpacity
                  onPress={() => {
                    navigation.navigate('Grabfood');
                  }}>
                  <View style={styles.Food}>
                    <Image
                      source={icFood}
                      style={{
                        width: 34,
                        height: 34,
                        resizeMode: 'contain',
                        alignSelf: 'center',
                      }}
                    />
                  </View>
                </TouchableOpacity>
                <Text
                  style={{
                    textAlign: 'center',
                  }}>
                  Makanan
                </Text>
              </View>
              {/* MENU 2 */}
              <View>
                <View style={styles.Bike}>
                  <Image
                    source={icBike}
                    style={{
                      width: 34,
                      height: 34,
                      resizeMode: 'contain',
                      alignSelf: 'center',
                    }}
                  />
                </View>
                <Text
                  style={{
                    textAlign: 'center',
                  }}>
                  Motor
                </Text>
              </View>
              <View>
                <View style={styles.Car}>
                  <Image
                    source={icCar}
                    style={{
                      width: 37,
                      height: 37,
                      resizeMode: 'contain',
                      alignSelf: 'center',
                    }}
                  />
                </View>
                <Text
                  style={{
                    textAlign: 'center',
                  }}>
                  Mobil
                </Text>
              </View>
              <View>
                <View style={styles.Box}>
                  <Image
                    source={icBox}
                    style={{
                      width: 34,
                      height: 34,
                      resizeMode: 'contain',
                      alignSelf: 'center',
                    }}
                  />
                </View>
                <Text
                  style={{
                    textAlign: 'center',
                  }}>
                  Box
                </Text>
              </View>
              <View>
                <View style={styles.Mart}>
                  <Image
                    source={icMart}
                    style={{
                      width: 34,
                      height: 34,
                      resizeMode: 'contain',
                      alignSelf: 'center',
                    }}
                  />
                </View>
                <Text
                  style={{
                    textAlign: 'center',
                  }}>
                  Mart
                </Text>
              </View>
              <View>
                <View style={styles.Pulsa}>
                  <Image
                    source={icPulsa}
                    style={{
                      width: 34,
                      height: 34,
                      resizeMode: 'contain',
                      alignSelf: 'center',
                    }}
                  />
                </View>
                <Text
                  style={{
                    textAlign: 'center',
                  }}>
                  Pulsa
                </Text>
              </View>

              <View>
                <View style={styles.Pulsa}>
                  <Image
                    source={icJastip}
                    style={{
                      width: 34,
                      height: 34,
                      resizeMode: 'contain',
                      alignSelf: 'center',
                    }}
                  />
                </View>
                <Text
                  style={{
                    textAlign: 'center',
                  }}>
                  Jastip
                </Text>
              </View>
              <View>
                <View style={styles.Pulsa}>
                  <Image
                    source={icDokter}
                    style={{
                      width: 34,
                      height: 34,
                      resizeMode: 'contain',
                      alignSelf: 'center',
                    }}
                  />
                </View>
                <Text
                  style={{
                    textAlign: 'center',
                  }}>
                  Kesehatan
                </Text>
              </View>

              <View>
                <View style={styles.Pulsa}>
                  <Image
                    source={icPaket}
                    style={{
                      width: 34,
                      height: 34,
                      resizeMode: 'contain',
                      alignSelf: 'center',
                    }}
                  />
                </View>
                <Text
                  style={{
                    textAlign: 'center',
                  }}>
                  Paket Diskon
                </Text>
              </View>
              <View>
                <View style={styles.Pulsa}>
                  <Image
                    source={icPromo}
                    style={{
                      width: 34,
                      height: 34,
                      resizeMode: 'contain',
                      alignSelf: 'center',
                    }}
                  />
                </View>
                <Text
                  style={{
                    textAlign: 'center',
                  }}>
                  Promo
                </Text>
              </View>
              <View>
                <View style={styles.Pulsa}>
                  <Image
                    source={icHotel}
                    style={{
                      width: 34,
                      height: 34,
                      resizeMode: 'contain',
                      alignSelf: 'center',
                    }}
                  />
                </View>
                <Text
                  style={{
                    textAlign: 'center',
                  }}>
                  Hotel
                </Text>
              </View>
              <View>
                <View style={styles.Pulsa}>
                  <Image
                    source={icTagihan}
                    style={{
                      width: 34,
                      height: 34,
                      resizeMode: 'contain',
                      alignSelf: 'center',
                    }}
                  />
                </View>
                <Text
                  style={{
                    textAlign: 'center',
                  }}>
                  Tagihan
                </Text>
              </View>

              <View>
                <View style={styles.Pulsa}>
                  <Image
                    source={icJasa}
                    style={{
                      width: 34,
                      height: 34,
                      resizeMode: 'contain',
                      alignSelf: 'center',
                    }}
                  />
                </View>
                <Text
                  style={{
                    textAlign: 'center',
                  }}>
                  Jasa Rumah
                </Text>
              </View>

              <View>
                <View style={styles.Pulsa}>
                  <Image
                    source={icSewa}
                    style={{
                      width: 34,
                      height: 34,
                      resizeMode: 'contain',
                      alignSelf: 'center',
                    }}
                  />
                </View>
                <Text
                  style={{
                    textAlign: 'center',
                  }}>
                  Sewa
                </Text>
              </View>

              <View>
                <View style={styles.Pulsa}>
                  <Image
                    source={icGift}
                    style={{
                      width: 34,
                      height: 34,
                      resizeMode: 'contain',
                      alignSelf: 'center',
                    }}
                  />
                </View>
                <Text
                  style={{
                    textAlign: 'center',
                  }}>
                  Hadiah
                </Text>
              </View>

              <View>
                <View style={styles.Pulsa}>
                  <Image
                    source={icAsuransi}
                    style={{
                      width: 34,
                      height: 34,
                      resizeMode: 'contain',
                      alignSelf: 'center',
                    }}
                  />
                </View>
                <Text
                  style={{
                    textAlign: 'center',
                  }}>
                  Asuransi
                </Text>
              </View>

              <View>
                <View style={styles.Pulsa}>
                  <Image
                    source={icGame}
                    style={{
                      width: 34,
                      height: 34,
                      resizeMode: 'contain',
                      alignSelf: 'center',
                    }}
                  />
                </View>
                <Text
                  style={{
                    textAlign: 'center',
                  }}>
                  Game TopUp
                </Text>
              </View>

              <View>
                <View style={styles.Pulsa}>
                  <Image
                    source={icPetualangan}
                    style={{
                      width: 34,
                      height: 34,
                      resizeMode: 'contain',
                      alignSelf: 'center',
                    }}
                  />
                </View>
                <Text
                  style={{
                    textAlign: 'center',
                  }}>
                  Petualangan
                </Text>
              </View>

              <View>
                <View style={styles.Pulsa}>
                  <Image
                    source={icOther}
                    style={{
                      width: 34,
                      height: 34,
                      resizeMode: 'contain',
                      alignSelf: 'center',
                    }}
                  />
                </View>
                <Text
                  style={{
                    textAlign: 'center',
                  }}>
                  Lainnya
                </Text>
              </View>
            </View>
          </ScrollView>
          {/* BALANCE */}
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              backgroundColor: 'white',
            }}>
            <View style={styles.balance}>
              <View
                style={{
                  justifyContent: 'center',
                  alignContent: 'center',
                }}>
                <Text style={{color: 'black'}}>Saldo</Text>
                <Text style={{fontWeight: 'bold', color: 'black'}}>
                  Rp 107.980
                </Text>
              </View>
              <Image
                source={icBalance1}
                style={{
                  width: 30,
                  height: 30,
                  resizeMode: 'contain',
                  alignSelf: 'center',
                }}
              />
            </View>

            <View style={styles.balance}>
              <View
                style={{
                  justifyContent: 'center',
                  alignContent: 'center',
                }}>
                <Text style={{color: 'black'}}>Point Tersedia</Text>
                <Text style={{fontWeight: 'bold', color: 'black'}}>518</Text>
              </View>
              <Image
                source={icBalance1}
                style={{
                  width: 30,
                  height: 30,
                  resizeMode: 'contain',
                  alignSelf: 'center',
                }}
              />
            </View>
          </View>
          {/* PESAN SEKARANG */}
          <View
            style={{
              backgroundColor: 'white',
            }}>
            <Text
              style={{
                color: 'black',
                marginLeft: 10,
                marginTop: 20,
                fontSize: 19,
                fontWeight: 'bold',
              }}>
              Pesan GrabExpress Lebih Hemat
            </Text>

            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}>
              <View
                style={{
                  flexDirection: 'row',
                }}>
                <Image
                  source={icPesan1}
                  style={{
                    height: 235,
                    width: Platform.OS == 'android' ? 380 : 360,
                    marginTop: 10,
                    marginLeft: 10,
                  }}
                />
                <Image
                  source={icPesan2}
                  style={{
                    height: 235,
                    width: Platform.OS == 'android' ? 380 : 360,
                    marginTop: 10,
                    marginLeft: 10,
                    marginRight: 10,
                  }}
                />
              </View>
            </ScrollView>
            <Text
              style={{
                fontWeight: 'bold',
                fontSize: 16,
                marginLeft: 10,
              }}>
              Dapatkan diskon spesial hingga 99%
            </Text>
            <Text
              style={{
                marginLeft: 10,
              }}>
              Sponsored by GrabExpress
            </Text>
            <View>
              <Text
                style={{
                  fontWeight: 'bold',
                  marginLeft: 10,
                  marginTop: 25,
                  fontSize: 15,
                  marginBottom: 10,
                }}>
                Restoran yang kamu sukai
              </Text>

              <ScrollView
                horizontal={true}
                showsHorizontalScrollIndicator={false}>
                <View
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginBottom: Platform.OS == 'android' ? 100 : 10,
                  }}>
                  <Image
                    source={icFood1}
                    style={{
                      width: 100,
                      height: 80,
                      marginLeft: 10,
                    }}
                  />
                  <Image
                    source={icFood2}
                    style={{
                      width: 100,
                      height: 80,
                      marginLeft: 10,
                    }}
                  />
                  <Image
                    source={icFood3}
                    style={{
                      width: 100,
                      height: 80,
                      marginLeft: 10,
                    }}
                  />
                  <Image
                    source={icFood4}
                    style={{
                      width: 100,
                      height: 80,
                      marginLeft: 10,
                    }}
                  />
                </View>
              </ScrollView>
            </View>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#2eb82e',
    // alignContent: 'center',
    // justifyContent:'center',
    // alignItems:'center'
  },
  box: {
    width: 0.1,
    height: 50,
    backgroundColor: 'white',
    top: 50,
    bottom: 50,
    marginBottom: 45,
  },
  search: {
    padding: 20,
    marginTop: -23,
    margin: 10,
    width: Platform.OS == 'ios' ? 320 : 340,
    height: 50,
    borderTopLeftRadius: 7,
    borderBottomLeftRadius: 7,
    backgroundColor: 'white',
    borderColor: 'black',
    borderWidth: 0.3,
    // justifyContent:'center',
    alignContent: 'center',
    flexDirection: 'row',
  },
  scan: {
    marginTop: -23,
    width: 50,
    height: 50,
    borderTopRightRadius: 7,
    borderBottomRightRadius: 7,
    backgroundColor: 'white',
    borderColor: 'black',
    // borderWidth:0.3,
    justifyContent: 'center',
    alignContent: 'center',
    marginLeft: -10,
    borderRightWidth: 0.3,
    borderBottomWidth: 0.3,
    borderTopWidth: 0.3,
  },
  Food: {
    marginTop: 20,
    width: 50,
    height: 50,
    backgroundColor: '#c8f0dd',
    borderColor: '#c8f0dd',
    borderWidth: 1,
    justifyContent: 'center',
    alignContent: 'center',
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 100,
  },
  Bike: {
    marginTop: 20,
    width: 50,
    height: 50,
    backgroundColor: '#c8f0dd',
    borderColor: '#c8f0dd',
    borderWidth: 1,
    justifyContent: 'center',
    alignContent: 'center',
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 100,
  },
  Car: {
    marginTop: 20,
    width: 50,
    height: 50,
    backgroundColor: '#c8f0dd',
    borderColor: '#c8f0dd',
    borderWidth: 1,
    justifyContent: 'center',
    alignContent: 'center',
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 100,
  },
  Box: {
    marginTop: 20,
    width: 50,
    height: 50,
    backgroundColor: '#c8f0dd',
    borderColor: '#c8f0dd',
    borderWidth: 1,
    justifyContent: 'center',
    alignContent: 'center',
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 100,
  },
  Mart: {
    marginTop: 20,
    width: 50,
    height: 50,
    backgroundColor: '#c8f0dd',
    borderColor: '#c8f0dd',
    borderWidth: 1,
    justifyContent: 'center',
    alignContent: 'center',
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 100,
  },
  Pulsa: {
    marginTop: 20,
    width: 50,
    height: 50,
    backgroundColor: '#c8f0dd',
    borderColor: '#c8f0dd',
    borderWidth: 1,
    justifyContent: 'center',
    alignContent: 'center',
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 100,
  },
  balance: {
    marginTop: 40,
    width: Platform.OS == 'android' ? 185 : 175,
    height: 50,
    borderWidth: 1,
    backgroundColor: '#e6e6e6',
    borderColor: '#e6e6e6',
    // borderWidth:0.3,
    justifyContent: 'space-between',
    alignContent: 'center',
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 7,
    flexDirection: 'row',
    padding: 10,
  },
});

export default Home;
